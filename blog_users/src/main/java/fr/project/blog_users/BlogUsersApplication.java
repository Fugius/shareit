package fr.project.blog_users;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BlogUsersApplication {

	public static void main(String[] args) {
		SpringApplication.run(BlogUsersApplication.class, args);
	}

}
