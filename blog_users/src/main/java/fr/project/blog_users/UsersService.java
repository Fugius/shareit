package fr.project.blog_users;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;



@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class UsersService {
	
	@Autowired
	UserRepository repo;
	
	private String HashPassword(String clear) {

        String generatedPassword = null;
        try {
            // Create MessageDigest instance for MD5
            MessageDigest md = MessageDigest.getInstance("MD5");
            //Add password bytes to digest
            md.update(clear.getBytes());
            //Get the hash's bytes 
            byte[] bytes = md.digest();
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
            generatedPassword = sb.toString();
        } 
        catch (NoSuchAlgorithmException e) 
        {
            e.printStackTrace();
        }
        return generatedPassword;

	}
	
	@PostConstruct
	public void initialize() {
	   User u = new User("admin", "21232f297a57a5a743894a0e4a801fc3", "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7c/User_font_awesome.svg/512px-User_font_awesome.svg.png");
	   repo.save(u);
	}
	
	@PostMapping("/users/create")
	public User createUser(@RequestBody User user) throws Exception {
		//if user name is available
		if (repo.findByName(user.getName()).isEmpty()) {
			user.setPasswd(HashPassword(user.getPasswd()));
			repo.save(user);
			
			User u = repo.findByName(user.getName()).get(0).copy();
			return u;
			
		} else { //else throw an exception
			throw new Exception("Name already taken ! ");
		}
	}
	
	@DeleteMapping("/users/friend")
	public void deleteUser(@RequestParam Long friend_id, @RequestParam Long user_id) {
		User u = repo.findById(user_id).get();
		User f = repo.findById(friend_id).get();
		
		u.removeFriend(f);
		f.removeFriend(u);
		repo.save(f);
		repo.save(u);
	}
	
	@GetMapping("/users")
	public User login(@RequestParam String name, @RequestParam String passwd) throws Exception {
		List<User> matching = repo.findByName(name);
		
		
		if (matching.isEmpty() || !matching.get(0).getPasswd().equals(HashPassword(passwd))) {
			throw new Exception("Incorrect password or user name ! ");
		} else {
			return matching.get(0).copy();
		}
	}
	
	@GetMapping("/users/info")
	public User getUser(@RequestParam Long id) {
		User u = repo.findById(id).get().copy();
		return u;
	}
	
	@GetMapping("users/friends")
	public List<User> getFriends(@RequestParam Long id) {
		User u_ = getUser(id);
		List<User> friends = u_.getFriends();
		List<User> cf = new ArrayList<User>();
		
		for (User f : friends) {
			cf.add(f.copy());
		}
		
		return cf;
	}
	
	@PutMapping("users/friends")
	public void addFriend(@RequestBody User u, @RequestParam String friendName) throws Exception {
		User u_ = repo.findById(u.getId()).get();
		if (u_.getName().equals(friendName)) {
			throw new Exception("You are already your best friend ! :D");
		}
		
		if (repo.findByName(friendName).isEmpty()) {
			throw new Exception("User does not exist !");
		}
		
		User friend = repo.findByName(friendName).get(0);
		
		List<User> friends = u_.getFriends();
		for (User a : friends) {
			if (a.getName().equals(friendName)) {
				return;
			}
		}
		
		friend.addFriend(u_);
		u_.addFriend(friend);
		repo.save(u_);
		repo.save(friend);
	}
	
	@PutMapping("users")
	public void updateUserInfos(@RequestBody User u) throws Exception {
		if (u == null) {
			return;
		}
		
		User u_ = repo.findById(u.getId()).get();
		
		System.out.println(u.getName());
		System.out.println(u_.getName());
		
		if (!u.getName().equals(u_.getName())) {
			if (repo.findByName(u.getName()).size() > 0) {
				throw new Exception("Name already taken !");
			}
		}
		
		if (u.getDescription() != null && u.getDescription().length() >= 2048) {
			throw new Exception("Description must be under 2048 characters !");
		}
		
		u_.setAvatar(u.getAvatar());
		u_.setDescription(u.getDescription());
		u_.setName(u.getName());
		
		if (!u.getPasswd().isEmpty()) {
			u_.setPasswd(HashPassword(u.getPasswd()));
		}
		repo.save(u_);
		
	}
}
