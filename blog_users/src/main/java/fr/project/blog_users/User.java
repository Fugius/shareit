package fr.project.blog_users;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class User {
	
	private long id;
	
	private String name;
	private String passwd;
	private String avatar_url;
	private String description;
	
	List<User> friends = new ArrayList<>();
	
	public User() {
		super();
	}

	public User(String n, String p, String ico) {
		super();
		
		name = n;
		passwd = p;
		avatar_url = ico;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long getId() {
		return id;
	}
	
	public void setId(long i) {
		id = i;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String n) {
		name = n;
	}
	
	public String getPasswd() {
		return passwd;
	}
	
	public void setPasswd(String p) {
		passwd = p;
	}
	
	public String getAvatar() {
		return avatar_url;
	}
	
	public void setAvatar(String a) {
		avatar_url = a;
	}
	
	public void addFriend(User u) {
		friends.add(u);
	}
	
	@ManyToMany
	@JsonIgnore
	public List<User> getFriends() {
		return friends;
	}
	
	public void removeFriend(User u) {
		if (friends.isEmpty()) {
			return;
		}
		
		User fr = null;
		
		for (User f : friends) {
			if (f.getId() == u.getId()) {
				fr = f;
			}
		}
		
		if (fr != null) {
			friends.remove(fr);
		}
	}
	
	public void setFriends(List<User> f) {
		friends = f;
	}
	
	public User copy() {
		User c = new User();
		c.setId(id);
		c.setAvatar(avatar_url);
		c.setFriends(friends);
		c.setName(name);
		c.setDescription(description);
		
		return c;
	}
	
	@Lob 
	@Column(name="CONTENT", length=2048)
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String d) {
		description = d;
	}
}
