import { Component, OnInit, Input } from '@angular/core';
import { HttpService } from "../http.service";


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  @Input() id: number;

  options = { autoHide: false, scrollbarMinSize: 100 };

  name = "";
  avatar = "";
  password = "";
  description = "";

  error_message = "";

  constructor(private http: HttpService) {
  }

  getUserInfos() {
    this.http.getUserInfos(this.id).subscribe(
      data => {
        this.name = data['name'];
        this.avatar = data['avatar'];
        this.description = data['description'];
      }
    );
  }

  ngOnInit(): void {
    this.getUserInfos();
  }

  updateDescription() {
    let desc = this.description;
    this.getUserInfos();
    this.description = desc;

    this.http.updateUserInfos(this.id, this.name, this.password, this.avatar, this.description).subscribe(data => { this.getUserInfos(); });
  }

  updateName() {
    let n = this.name;
    this.getUserInfos();
    this.name = n;

    this.http.updateUserInfos(this.id, this.name, this.password, this.avatar, this.description).subscribe(
      data => { this.getUserInfos(); },
      error => { this.error_message = error.error.message;}
    );
    this.getUserInfos();
  }

  updateAvatar() {
    let a = this.avatar;
    this.getUserInfos();
    this.avatar = a;

    this.http.updateUserInfos(this.id, this.name, this.password, this.avatar, this.description).subscribe(data => { this.getUserInfos(); });
    this.getUserInfos();
  }

  updatePassword() {
    let p = this.password;
    this.getUserInfos();
    this.password = p;

    this.http.updateUserInfos(this.id, this.name, this.password, this.avatar, this.description).subscribe(data => { this.getUserInfos(); });
    this.getUserInfos();
  }

}
