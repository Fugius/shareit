import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PostComponent } from './post/post.component';
import { FormsModule } from '@angular/forms'

import { HttpClientModule } from '@angular/common/http';
import { PortalComponent } from './portal/portal.component';
import { UserComponent } from './user/user.component';
import { FriendsComponent } from './friends/friends.component';
import { ListPostComponent } from './list-post/list-post.component';
import { DetailedPostComponent } from './detailed-post/detailed-post.component';
import { YoutubePipe } from './youtube.pipe'


@NgModule({
  declarations: [
    AppComponent,
    PostComponent,
    PortalComponent,
    UserComponent,
    FriendsComponent,
    ListPostComponent,
    DetailedPostComponent,
    YoutubePipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
