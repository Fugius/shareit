import { Component, OnInit } from '@angular/core';
import { Input, Output, EventEmitter } from '@angular/core';
import { HttpService } from '../http.service'

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {
  @Input() id: Number;
  @Output() onClose: EventEmitter<boolean> = new EventEmitter();

  post_content = "";
  post_title = "";
  post_date = "";

  constructor(private http: HttpService) { }

  ngOnInit(): void {
  }

  createPost() {
    this.post_date = Date().toString();
    this.http.createPost(this.post_content, this.post_title, this.post_date, this.id).subscribe();
    this.onClose.emit();
  }

}
