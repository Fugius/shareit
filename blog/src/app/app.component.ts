import { Component, OnInit } from '@angular/core';
import { HttpService } from "./http.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  id = -1;

  userWindow = false;
  friendsWindow = false;
  postWindow = false;
  newPostWindow = false;

  search = "";

  constructor(private _http: HttpService) { }
  getId($event) {
    this.id = $event;
    this.postWindow = true;
  }

  ngOnInit(): void {
  }

  onSearch() {
    this.postWindow = false;
    setTimeout(() => { this.postWindow = true;  }, 1);
  }

  showUserWindow() {
    this.userWindow = true;
    this.friendsWindow = false;
    this.postWindow = false;
    this.newPostWindow = false;
  }

  showFriendWindow() {
    this.friendsWindow = true;
    this.userWindow = false;
    this.postWindow = false;
    this.newPostWindow = false;
  }

  showPostWindow() {
    this.postWindow =  true;
    this.friendsWindow = false;
    this.userWindow = false;
    this.newPostWindow = false;
  }

  showNewPostWindow() {
    this.newPostWindow = true;
    this.friendsWindow = false;
    this.userWindow = false;
    this.postWindow = false;
  }

  hideAll() {
    this.id = -1;
    this.friendsWindow = false;
    this.userWindow = false;
    this.postWindow = false;
    this.newPostWindow = false;
  }
}
