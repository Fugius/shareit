import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { HttpService } from "../http.service";

@Component({
  selector: 'app-portal',
  templateUrl: './portal.component.html',
  styleUrls: ['./portal.component.scss']
})
export class PortalComponent implements OnInit {
  @Output() UserId = new EventEmitter<Number>();

  createAccount_ = false;

  name = "";
  passwd = "";
  avatar = "";

  errorMessage = "";

  constructor(private _http: HttpService) { }

  ngOnInit(): void {
  }

  connect() {
    this._http.connect(this.name, this.passwd).subscribe(
      data => this.UserId.emit(data['id']),
      error => this.errorMessage = error.error.message
    );
  }

  setCreateAccount(b) {
    this.createAccount_ = b;
  }

  createAccount() {
    this._http.createAccount(this.name, this.passwd, this.avatar).subscribe(
      data => this.UserId.emit(data['id']),
      error => this.errorMessage = error.error.message
    );
  }

}
