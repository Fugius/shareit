import { Component, OnInit, Input } from '@angular/core';
import { HttpService } from '../http.service'

@Component({
  selector: 'app-friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.scss']
})
export class FriendsComponent implements OnInit {
  @Input() id: number;
  friendName = "";

  friends = [];
  view_num = -1;

  constructor(private http: HttpService) { }

  update() {
    this.friends = [];
    this.http.getFriends(this.id).subscribe(
      data => {
        for (let f in data) {
          if (data[f].description == null) {
            data[f].description = "";
          }
          this.friends.push(data[f]);
        }

      }
    );
  }

  ngOnInit(): void {
    this.update();
  }

  addFriend() {
    this.http.addFriend(this.id, this.friendName).subscribe(data => {
      this.update();
    });
  }

  showDetails(id) {
    if (id < 0) {
      this.view_num = id;
      return;
    }

    for (let i = 0; i < this.friends.length; i++) {
      if (this.friends[i].id == id) {
        this.view_num = i;
      }
    }
  }

  deleteFriend(id_) {
    this.http.delFriend(this.friends[id_].id, this.id).subscribe(data => {
      this.view_num = -1;
      this.update();
    });
  }

}
