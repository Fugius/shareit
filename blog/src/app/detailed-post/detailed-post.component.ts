import { Component, OnInit, Input } from '@angular/core';
import { HttpService } from '../http.service'
import { YoutubePipe } from '../youtube.pipe'

@Component({
  selector: 'app-detailed-post',
  templateUrl: './detailed-post.component.html',
  styleUrls: ['./detailed-post.component.scss']
})
export class DetailedPostComponent implements OnInit {
  @Input() postId;
  @Input() userId;

  post;
  author = {
    name: "",
    avatar: ""
  };

  answer = null;
  comments = [];

  comment_content="";

  constructor(private http: HttpService) { }

  replyToComment() {
    this.http.replyToComment(this.comment_content, this.userId, this.postId, this.answer.id).subscribe(
      data => {
        this.comments = [];
        this.getComments();
        this.comment_content = "";
        this.answer = null;
      }
    );
  }

  postComment() {
    if (this.comment_content == "") {
      return;
    }

    this.http.createComment(this.comment_content, this.userId, this.postId).subscribe(
      data => {
        this.comments = [];
        this.getComments();
        this.comment_content = "";
      }
    );
  }

  getUserInfos(msg) {
    this.getUser(msg.userId).subscribe(
      data => {
        msg.userName = data['name'];
        msg.userAvatar = data['avatar'];

        if (msg.userAvatar == null || msg.userAvatar == "") {
          msg.userAvatar = "/assets/error.png";
        }

        for (let j in msg.replies) {
          this.getUserInfos(msg.replies[j]);
        }

        if (!msg.hasParent) {
          this.comments.push(msg);
          this.comments.sort((a, b) => {return a.id - b.id});
        }
      }
    );

  }

  getComments() {
    this.http.getComments(this.postId).subscribe(
      data => {
        for (let i in data) {
          let a = data[i];
          this.getUserInfos(a);
        }
      }
    );
  }

  getUser(id) {
    return this.http.getUserInfos(id);
  }

  ngOnInit(): void {
    this.http.getPost(this.postId).subscribe(
      data => {
        this.post = data;
        this.post.content = this.post.content.replace("\\n", "<br><br>");

        this.getUser(this.post.writerId).subscribe(data => {
          this.author.name = data['name'];
          this.author.avatar = data['avatar'];

          if (data['avatar'] == null || data['avatar'] == "") {
            this.author.avatar = "/assets/error.png";
          }

          this.post.elapsed = this.formatTimeElapsed(new Date(this.post.date));
        });
      }
    );

    this.comments = [];
    this.getComments();
  }

  formatTimeElapsed(a: Date) {
    let e = new Date().getTime() - a.getTime();
    let seconds = e/1000;
    let minutes = (seconds - (seconds % 60)) / 60;
    seconds = Math.round(seconds % 60);
    let hours = (minutes - (minutes % 60)) / 60;
    minutes = Math.round(minutes % 60);
    let days = (hours - (hours % 24)) / 24;
    days = Math.round(days % 24);
    let str = "";

    if (days > 0) {
      str += " " + days.toString() + " days";
    }

    if (hours > 0) {
      str += " " + hours.toString() + " hours";
    }

    if (minutes > 0) {
      str += " " + minutes.toString() + " minutes";
    }

    if (seconds > 0) {
      str += " " + seconds.toString() + " seconds";
    }

    return str;
  }

}
