import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { HttpParams } from '@angular/common/http'
import { HttpHeaders } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  userService = "http://localhost:8080";
  postService = "http://localhost:8081";
  commentsService = "http://localhost:8082";

  constructor(private http: HttpClient) { }

  connect(username, password) {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    let params = new HttpParams().set("name", username).set("passwd", password);

    return this.http.get(this.userService + '/users', {headers: headers, params: params});
  }

  createAccount(username, password, avatar) {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    let data = {
      name: username,
      passwd: password,
      avatar: avatar
    };

    return this.http.post(this.userService + '/users/create', data, {headers: headers});
  }

  updateUserInfos(id, username, password, avatar, description) {
    let headers = new HttpHeaders();
    let data = {
      id: id,
      name: username,
      passwd: password,
      avatar: avatar,
      description: description
    };

    return this.http.put(this.userService + '/users', data, {headers: headers});
  }

  getFriends(id) {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    let params = new HttpParams().set("id", id);

    return this.http.get(this.userService + '/users/friends', {headers: headers, params: params});
  }

  addFriend(id, friendName) {
    let headers = new HttpHeaders();
    let params = new HttpParams().set("friendName", friendName);
    let data = {
      id: id,
    };

    return this.http.put(this.userService + '/users/friends', data, {headers: headers, params:params});
  }

  getUserInfos(id) {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    let params = new HttpParams().set("id", id);

    return this.http.get(this.userService + '/users/info', {headers: headers, params: params});
  }

  delFriend(friend_id, user_id) {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    let params = new HttpParams().set("user_id", user_id).set("friend_id", friend_id);

    return this.http.delete(this.userService + '/users/friend', {headers: headers, params: params});
  }

  createPost(content, title, date, id) {
    let headers = new HttpHeaders();
    let data = {
      writerId: id,
      name: title,
      content: content,
      date: date
    };

    return this.http.post(this.postService + '/posts/new', data, {headers: headers});
  }

  getUserPosts(id, keyword) {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    let params = new HttpParams().set("id", id).set("keyword", keyword);


    return this.http.get(this.postService + '/posts/user', {headers: headers, params: params});
  }

  getPost(id) {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    let params = new HttpParams().set("id", id);

    return this.http.get(this.postService + '/posts', {headers: headers, params: params});
  }

  getComments(postId) {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    let params = new HttpParams().set("id", postId);

    return this.http.get(this.commentsService + '/comments/by_post', {headers: headers, params: params});
  }

  createComment(content, userId, postId) {
    let headers = new HttpHeaders();
    let data = {
      userId: userId,
      postId: postId,
      content: content
    };

    return this.http.post(this.commentsService + '/comments/new', data, {headers: headers});
  }

  replyToComment(content, userId, postId, parentId) {
    let headers = new HttpHeaders();
    let params = new HttpParams().set("id", parentId);
    let data = {
      userId: userId,
      postId: postId,
      content: content
    };

    return this.http.put(this.commentsService + '/comments', data, {headers: headers, params:params});
  }
}
