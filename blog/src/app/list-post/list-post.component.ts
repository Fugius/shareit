import { Component, OnInit, Input } from '@angular/core';
import { HttpService } from '../http.service'

@Component({
  selector: 'app-list-post',
  templateUrl: './list-post.component.html',
  styleUrls: ['./list-post.component.scss']
})
export class ListPostComponent implements OnInit {
  @Input() id: Number;
  @Input() select: String;

  posts = [];
  showPost = null;

  constructor(private http: HttpService) { }

  ngOnInit(): void {
    this.getFriendsPosts();
  }

  formatTimeElapsed(a: Date) {
    let e = new Date().getTime() - a.getTime();
    let seconds = e/1000;
    let minutes = (seconds - (seconds % 60)) / 60;
    seconds = Math.round(seconds % 60);
    let hours = (minutes - (minutes % 60)) / 60;
    minutes = Math.round(minutes % 60);
    let days = (hours - (hours % 24)) / 24;
    days = Math.round(days % 24);
    let str = "";

    if (days > 0) {
      str += " " + days.toString() + " days";
    }

    if (hours > 0) {
      str += " " + hours.toString() + " hours";
    }

    if (minutes > 0) {
      str += " " + minutes.toString() + " minutes";
    }

    if (seconds > 0 || days + hours + minutes + seconds == 0) {
      str += " " + seconds.toString() + " seconds";
    }

    return str;
  }

  sortPosts() {
    this.posts = this.posts.sort((a, b) => {
      let x = new Date(a.date);
      let y = new Date(b.date);
      return y.getTime() - x.getTime();
    });

    for (let i in this.posts) {
      this.http.getUserInfos(this.posts[i].writerId).subscribe(data => {
        this.posts[i].userName = data['name'];
      })
    }

    for (let i in this.posts) {
      this.posts[i].elapsed = this.formatTimeElapsed(new Date(this.posts[i].date));
    }

  }

  getOwnPosts() {
    this.http.getUserPosts(this.id, this.select).subscribe(data => {
      this.posts = this.posts.concat(data);
      this.sortPosts();
    });
  }

  getFriendsPosts() {
    this.http.getFriends(this.id).subscribe((data: any[]) => {
      if (data != null && data.length == 0) {
        this.getOwnPosts();
      } else{
        for (let f in data) {
          this.http.getUserPosts(data[f].id, this.select).subscribe(data_ => {
            this.posts = this.posts.concat(data_);
            if (f == Object.keys(data)[Object.keys(data).length-1]) {
              this.getOwnPosts();
            }
          });
        }
      }

    });
  }

}
