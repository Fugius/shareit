package fr.project.blog_com;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface MessageRepository extends CrudRepository<Message, Long> {
	public List<Message> findByUserId(Long UserId);
	public List<Message> findByPostId(Long PostId);
}
