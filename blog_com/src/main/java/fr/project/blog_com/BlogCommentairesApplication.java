package fr.project.blog_com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BlogCommentairesApplication {

	public static void main(String[] args) {
		SpringApplication.run(BlogCommentairesApplication.class, args);
	}

}
