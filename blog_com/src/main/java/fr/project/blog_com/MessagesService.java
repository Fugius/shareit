package fr.project.blog_com;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class MessagesService {
	
	@Autowired
	MessageRepository repo;
	
	@PostMapping("/comments/new")
	public void postComment(@RequestBody Message m) {
		repo.save(m);
	}
	
	@PutMapping("/comments")
	public void replyToComment(@RequestBody Message reply, @RequestParam long id) {
		Message Parent = repo.findById(id).get();
		Parent.addReply(reply);
		repo.save(Parent);
		
		repo.save(reply);
	}
	
	@GetMapping("/comments/by_post")
	public List<Message> getMessagesByPost(@RequestParam Long id) {
		List<Message> res = repo.findByPostId(id);		
		return res;	
	}
	
	@GetMapping("/comments/by_user")
	public List<Message> getMessagesByUser(@RequestParam Long id) {
		return repo.findByUserId(id);
	}
}
