package fr.project.blog_com;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Message {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	private long userId;
	private long postId;
	
	@Lob 
	@Column(name="CONTENT", length=2048)
	private String content;
	
	@OneToMany(mappedBy="parentMessage")
	private List<Message> replies;
	
	@ManyToOne
	@JsonIgnore
	private Message parentMessage;
	
	private boolean hasParent;
	
	public Message() {
		super();
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(long i) {
		id = i;
	}
	
	public long getUserId() {
		return userId;
	}
	
	public void setUserId(long uid) {
		userId = uid;
	}
	
	public long getPostId() {
		return postId;
	}
	
	public void setPostId(long pid) {
		postId = pid;
	}
	
	public String getContent() {
		return content;
	}
	
	public void setContent(String c) {
		content = c;
	}
	
	public List<Message> getReplies() {
		return replies;
	}
	
	public void setReplies(List<Message> r) {
		replies = r;
	}
	
	public void addReply(Message m) {
		replies.add(m);
		m.setParentMessage(this);
	}
	
	public Message getParentMessage() {
		return parentMessage;
	}
	
	public void setParentMessage(Message m) {
		parentMessage = m;
		hasParent = true;
	}
	
	public boolean getHasParent() {
		return hasParent;
	}
	
	public void setHasparent(boolean p) {
		hasParent = p;
	}

	

}
