package fr.project.blog_post;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class PostService {	
	
	@Autowired
	PostRepository repo;
	
	@PostMapping("/posts/new")
	public void createPost(@RequestBody Post post) throws Exception {
		repo.save(post);
	}
	
	@DeleteMapping("/posts")
	public void deletePost(@RequestBody Post post) {
		repo.delete(post);
	}
	
	@GetMapping("/posts")
	public Post findById(@RequestParam Long id) {
		if (repo.findAll().isEmpty()) {
			return null;
		}
		return repo.findById(id).get();
	}
	
	@GetMapping("/posts/name")
	public List<Post> findByName(@RequestParam String str) {
		List<Post> result = repo.findByName(str);	
		return result;
		
	}
	
	@GetMapping("/posts/user")
	public List<Post> findByUser(@RequestParam Long id, @RequestParam String keyword) {
		if (keyword == null || keyword.isEmpty()) {
			List<Post> all = repo.findByWriterId(id);
			return all;
		} else {
			List<Post> all = repo.findByWriterId(id);
			List<Post> res = new ArrayList<Post>();
			
			for (Post p : all) {
				if (p.getName().toUpperCase().contains(keyword.toUpperCase()) || p.getContent().toUpperCase().contains(keyword.toUpperCase())) {
					res.add(p);
				}
			}
			
			return res;
		}

	}
	
	@PutMapping("/posts")
	public void updatePostContent(@RequestBody Post post) {
		Post post_ = repo.findById(post.getId()).get();
		post_.setContent(post.getContent());
		repo.save(post_);
	}
	

}
