package fr.project.blog_post;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

@Entity
public class Post {
	private long id;
	private long writerid;
	
	private String content;
	private String name;
	
	private String date;
	
	public Post() {
		
	}
	
	public Post(long writerid, String content, String name) {
		this.content = content;
		this.writerid = writerid;
		this.content = content;
		this.name = name;
		
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long getId() {
		return id;
	}
	
	public void setId(long i) {
		id = i;
	}
	
	public long getWriterId() {
		return writerid;
	}
	
	public void setWriterId(long i) {
		writerid = i;
	}
	
	@Lob 
	@Column(name="CONTENT", length=32768)
	public String getContent() {
		return content;
	}
	
	public void setContent(String c) {
		content = c;
	}
	
	public void setName(String n) {
		name = n;
	}
	
	public String getName() {
		return name;
	}
	
	public String getDate() {
		return date;
	}
	
	public void setDate(String d) {
		date = d;
	}

}
