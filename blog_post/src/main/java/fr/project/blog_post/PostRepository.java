package fr.project.blog_post;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface PostRepository extends CrudRepository<Post, Long> {
	List<Post> findByName(String name);
	List<Post> findByWriterId(Long id);
	List<Post> findAll();
}
